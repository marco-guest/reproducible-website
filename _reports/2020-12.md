---
layout: report
year: "2020"
month: "12"
title: "Reproducible Builds in December 2020"
draft: false
date: 2021-01-05 15:41:18
---

[![]({{ "/images/reports/2020-12/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Greetings and welcome to the December 2020 report from the [Reproducible Builds](https://reproducible-builds.org) project.** In these monthly reports, we try to outline  most important things that have happened in and around the Reproducible Builds project.

<br>

[![]({{ "/images/reports/2020-12/solarwinds.jpg#right" | relative_url }})](https://www.washingtonpost.com/technology/2020/12/14/russia-hack-us-government/)

In mid-December, it was announced that there was a substantial and wide-reaching supply-chain attack that targeted many departments of the United States government including the [Treasury](https://home.treasury.gov/), [Commerce](https://www.commerce.gov/) and [Homeland Security](https://www.dhs.gov/) (DHS). The attack, informally known as 'SolarWinds' after the manufacturer of the network management software that was central to the compromise, was [described by the Washington Post](https://www.washingtonpost.com/technology/2020/12/14/russia-hack-us-government/) as:

> The far-reaching Russian hack that sent U.S. government and corporate officials scrambling in recent days appears to have been a quietly sophisticated bit of online spying. Investigators at cybersecurity firm FireEye, which itself was victimized in the operation, marveled that the meticulous tactics involved “some of the best operational security” its investigators had seen, using at least one piece of malicious software never previously detected.

This revelation is extremely relevant to Reproducible Builds project because, according to the [SANS Institute](https://www.sans.org), it appears that the source code and distribution systems were not compromised — instead, the *build system* was, and is therefore **precisely the kind of attack that reproducible builds is designed to prevent**. The *SolarWinds* attack is further evidence that reproducible builds is important and that it becomes a pervasive software engineering principle.

[![]({{ "/images/reports/2020-12/kimzetter.jpg#right" | relative_url }})](https://twitter.com/KimZetter/status/1338305089597964290)

More information on the attack may be found on [CNN](https://www.cnn.com/2020/12/16/tech/solarwinds-orion-hack-explained/index.html), [CSO](https://www.csoonline.com/article/3601508/solarwinds-supply-chain-attack-explained-why-organizations-were-not-prepared.html) [ComputerWeekly](https://www.computerweekly.com/news/252493662/SolarWinds-cyber-attack-How-worried-should-I-be-and-what-do-I-do-now), [BBC News](https://www.bbc.co.uk/news/technology-55368213), etc., and David A. Wheeler [started a discussion on our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2020-December/002109.html). [Kim Setter](https://twitter.com/KimZetter), author of [*Countdown to Zero Day: Stuxnet and the Launch of the World's First Digital Weapon*](https://www.penguinrandomhouse.com/books/219931/countdown-to-zero-day-by-kim-zetter/), [posted on Twitter](https://twitter.com/KimZetter/status/1338305089597964290) that:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I have report from Microsoft about SolarWinds hack, including IoCs. Excerpts in this thread: &quot;Microsoft security researchers recently discovered a sophisticated attack where an adversary inserted malicious code into a supply chain development process....</p>&mdash; Kim Zetter (@KimZetter) <a href="https://twitter.com/KimZetter/status/1338305089597964290?ref_src=twsrc%5Etfw">December 14, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<hr>

[![]({{ "/images/reports/2020-12/cctg.png#right" | relative_url }})](https://codeberg.org/corona-contact-tracing-germany/cwa-android/)

[Last month]({{ "/reports/2020-11/" | relative_url }}), we reported on a fork of the official German Corona App called '[*Corona Contact Tracing Germany*](https://codeberg.org/corona-contact-tracing-germany/cwa-android/)'. Since then, the application has been made available on the [F-Droid free-software application store](https://f-droid.org/packages/de.corona.tracing/). The app is not using the proprietary Google exposure notification framework, but a free software reimplementation by the [microG project](https://microg.org/), staying fully compatible with the official app. The version on *F-Droid* also supports reproducible builds, and instructions on [how to rebuild the package](https://codeberg.org/corona-contact-tracing-germany/cwa-android/src/branch/main/docs/rebuilding.md) are available from the [upstream Git repository](https://codeberg.org/corona-contact-tracing-germany/cwa-android/src/branch/main).&nbsp;([FSFE's announcement.](https://fsfe.org/news/2020/news-20201208-01.en.html))

[![]({{ "/images/reports/2020-12/maven.png#right" | relative_url }})](https://lists.reproducible-builds.org/pipermail/rb-general/2020-December/002094.html)

The [Reproducible Central](https://github.com/jvm-repo-rebuild/reproducible-central) project is an attempt to rebuild binaries published to the [Maven Central Repository](https://www.tutorialspoint.com/maven/maven_repositories.htm) in a reproducibility context. This month, [Hervé Boutemy](https://github.com/hboutemy) announced that *Reproducible Central* [was able to successfully rebuild the 100th reproducible release of a project published to the Maven Central Repository](https://lists.reproducible-builds.org/pipermail/rb-general/2020-December/002094.html) (and [counting](https://github.com/jvm-repo-rebuild/reproducible-central#rebuild-results)...).

We also first wrote about the [Threema](https://threema.ch/) messaging application in [September 2020]({{ "/reports/2020-09/" | relative_url }}). This month, however, the Threema developers announced that their applications have been released under the [GNU Affero General Public License (AGPL)](https://www.gnu.org/licenses/agpl-3.0.en.html) ([ announcement](https://threema.ch/en/blog/posts/open-source-and-new-partner)) and that [they are now reproducible](https://threema.ch/en/open-source/reproducible-builds) from version `4.5-beta1` onwards.&nbsp;([Spiegel.de announcement.](https://www.spiegel.de/netzwelt/apps/threema-ist-jetzt-eine-open-source-app-a-fff7eb90-8eba-4761-af55-581ae36bbcdc))

## Community news

Vagrant Cascadian announced that there will be another Reproducible Builds 'office hours' session on **Thursday January 7th**, where members of the Reproducible Builds project will be available to answer any questions.&nbsp;([More info.](https://lists.reproducible-builds.org/pipermail/rb-general/2020-December/002100.html))

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/), Jeremiah Orians sent a [brief status update](https://lists.reproducible-builds.org/pipermail/rb-general/2020-December/002114.html) on the status of the [*Bootstrappable*](https://bootstrappable.org/) project, noting that it is now possible to build a C compiler requiring "nothing outside of a POSIX kernel". Bernhard M. Wiedemann also [published the minutes of a recent debugging-oriented meeting](https://lists.reproducible-builds.org/pipermail/rb-general/2020-December/002099.html).

[![]({{ "/images/reports/2020-12/sfconservancy-projects.png#right" | relative_url }})](https://sfconservancy.org/blog/2020/dec/21/RB-CL-interview/)

Chris Lamb recently took part in an interview with an intern at the [Software Freedom Conservancy](https://sfconservancy.org/) to talk about the [Reproducible Builds](https://reproducible-builds.org/) project and the importance of reproducibility in software development:

> **VB**: How would you relate the importance of reproducibility to a user who is non-technical?

> **CL**: I sometimes use the analogy of the food ‘supply chain’ to quickly relate our work to non-technical audiences. The multiple stages of how our food reaches our plates today (such as seeding, harvesting, picking, transportation, packaging, etc.) can loosely translate to how software actually ends up on our computers, particularly in the way that if any of the steps in the multi-stage food supply chain has an issue then it quickly becomes a serious problem.

The full interview can be found [on the Conservancy webpages](https://sfconservancy.org/blog/2020/dec/21/RB-CL-interview/).

## Distributions

### [openSUSE](https://www.opensuse.org)

[![]({{ "/images/reports/2020-12/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

[Adrian Schröter](https://www.storch-bleckmar.de/blog/index.php/de/) added an option to the scripts powering the [Open Build Service](https://github.com/openSUSE/obs-build) to enable deterministic filesystem ordering. Whilst this degrades performance slightly, it also enables dozens of packages in [openSUSE Tumbleweed](https://software.opensuse.org/distributions/tumbleweed) to become reproducible.&nbsp;[[...](https://github.com/openSUSE/obs-build/pull/634)]
Also, Bernhard M. Wiedemann published his [monthly Reproducible Builds status update](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/message/VC5SOBSPKCAF5SCI3AE3SD5FZTNTYYOT/) for openSUSE Tumbleweed.

### [Debian](https://debian.org/)

[![]({{ "/images/reports/2020-12/debian.png#right" | relative_url }})](https://debian.org/)

In Debian, Holger Levsen uploaded 540 packages to the *unstable* distribution that were missing `.buildinfo` files for `Architecture: all` packages. Holger described his rationale and approach in a blog post titled [*On doing 540 no-source-change source-only uploads in two weeks*](http://layer-acht.org/thinking/blog/20201231-no-source-change-source-uploads/), and also he posted the [full list of packages he intends to upload during January 2021](https://lists.debian.org/debian-devel/2020/12/msg00419.html) to the [`debian-devel`](https://lists.debian.org/debian-devel/) mailing list:

> There are many binary (and source) packages in Debian which were uploaded before 2016 (which is when `.buildinfo` files were introduced) or were uploaded with binaries until that change in release policy July 2019.
>
> Ivo De Decker scheduled binNMUs for all the affected packages but due to the way binNMUs work, he couldn't do anything about `arch:all` packages as they currently cannot be rebuilt with binNMUs.

In recent months, Debian Developer [Stuart Prescott](https://nanonanonano.net/) has been improving [`python-debian`](https://salsa.debian.org/python-debian-team/python-debian), a Python library that is used to parse Debian-specific files such as changelogs, `.dscs`, etc. In particular, Stuart has been working on [adding support for `.buildinfo` files](https://bugs.debian.org/875306) used for recording reproducibility-related build metadata. This month, however, Stuart uploaded *python-debian* version `0.1.39` with many changes, including adding a type for `.buildinfo` files ([#875306](https://bugs.debian.org/875306)).

Chris Lamb identified two new issues ([`timestamps_in_3d_files_created_by_survex`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/e7d73123) & [`build_path_in_direct_url_json_file_generated_by_flit`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/86903835)), and Vagrant Cascadian discovered four [ecbuild](https://github.com/ecmwf/ecbuild)-related issues ([`records_build_flags_from_ecbuild`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/f8436336), [`captures_kernel_version_via_ecbuild`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/990c7af7), [`captures_build_arch_via_ecbuild`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/351273b8) & [`timestamps_in_h_generated_by_ecbuild`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/4c84467f)). 94 reviews of Debian packages were added, 84 were updated and 34 were removed this month, adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html).

Vagrant Cascadian made a large number of uploads to Debian fix a number of reproducible issues in packages that do not have an owner, including [`a2ps`](https://tracker.debian.org/pkg/a2ps) (`4.14-6`), [`autoconf`](https://tracker.debian.org/pkg/autoconf) (`2.69-13` & `2.69-14`), [`calife`](https://tracker.debian.org/pkg/calife) (`3.0.1-6`), [`coinor-symphony`](https://tracker.debian.org/pkg/coinor-symphony) (`5.6.16+repack1-3`), [`epm`](https://tracker.debian.org/pkg/epm) (`4.2-9` & `4.2-10`), [`grap`](https://tracker.debian.org/pkg/grap) (`1.45-4`), [`hpanel`](https://tracker.debian.org/pkg/hpanel) (`0.3.2-7`), [`libcommoncpp2`](https://tracker.debian.org/pkg/libcommoncpp2) (`1.8.1-9` & `1.8.1-10`), [`libdigidoc`](https://tracker.debian.org/pkg/libdigidoc) (`3.10.5-2`), [`libnss-ldap`](https://tracker.debian.org/pkg/libnss-ldap) (`265-6`), [`lprng`](https://tracker.debian.org/pkg/lprng) (`3.8.B-5`), [`magicfilter`](https://tracker.debian.org/pkg/magicfilter) (`1.2-66`), [`massif-visualizer`](https://tracker.debian.org/pkg/massif-visualizer) (`0.7.0-2`), [`milter-greylist`](https://tracker.debian.org/pkg/milter-greylist) (`4.6.2-2`), [`minlog`](https://tracker.debian.org/pkg/minlog) (`4.0.99.20100221-7`), [`mp3blaster`](https://tracker.debian.org/pkg/mp3blaster) (`3.2.6-2`), [`nis`](https://tracker.debian.org/pkg/nis) (`3.17.1-6` & `3.17.1-8`), [`spamassassin-heatu`](https://tracker.debian.org/pkg/spamassassin-heatu) (`3.02+20101108-4`), [`webauth`](https://tracker.debian.org/pkg/webauth) (`4.7.0-8`) & [`wily`](https://tracker.debian.org/pkg/wily) (`0.13.41-9` & `0.13.41-10`).

Similarly, Chris Lamb made two uploads of the [`sendfile`](https://tracker.debian.org/pkg/sendfile) package.

### [NixOS](https://nixos.org/)

[![]({{ "/images/reports/2020-12/nixos.png#right" | relative_url }})](https://nixos.org/)

NixOS made good progress towards having all packages required to build the minimal installation ISO image reproducible. Remaining work includes the [`python`](https://github.com/NixOS/nixpkgs/issues/107951), [`isl`](https://github.com/NixOS/nixpkgs/issues/106587) and [`gcc9`](https://github.com/NixOS/nixpkgs/issues/108475) packages and removing the use of Python 2.x in [`asciidoc`](https://github.com/NixOS/nixpkgs/pull/102398).

Elsewhere in NixOS, Adam Hoese of [tweag.io](https://www.tweag.io) also announced [*trustix*](https://www.tweag.io/blog/2020-12-16-trustix-announcement/), an [NGI Zero PET](https://nlnet.nl/PET)-funded initiative to provide infrastructure for sharing and enforcing reproducibility results for Nix-based systems.

Finally, the following NixOS-specific changes were made:

* Arnout Engelen:

    * [`compress-man-pages`](https://github.com/NixOS/nixpkgs/pull/105818) (create symlinks deterministically)
    * [`git`](https://github.com/NixOS/nixpkgs/pull/105498) (reproducible manual)
    * [`libseccomp`](https://github.com/NixOS/nixpkgs/pull/106644) (filesystem dates and ordering)
    * [`linux`](https://github.com/NixOS/nixpkgs/pull/106648) (omit build ID)
    * [`pytest`](https://github.com/NixOS/nixpkgs/pull/107825) (removed unreproducible test artifacts from the `pytest` package)
    * [`rustc`](https://github.com/NixOS/nixpkgs/pull/106284) (generate deterministic manifest)
    * [`setuptools`](https://github.com/NixOS/nixpkgs/pull/105680) (stable file ordering for `sdist`)
    * [`talloc`](https://github.com/NixOS/nixpkgs/pull/106646) (avoid Python 2.x build dependency)

* Atemu:

    * [`linux`](https://github.com/NixOS/nixpkgs/pull/107625) (disable module signing)

## Tools

[![]({{ "/images/reports/2020-12/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our project in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it provides human-readable diffs from many kinds of binary format. This month, Chris Lamb made the following changes, including releasing [version 163](https://diffoscope.org/news/diffoscope-163-released/) on multiple platforms:

* New features & bug fixes:

    * Normalise `ret` to `retq` in [`objdump`](https://sourceware.org/binutils/docs/binutils/objdump.html) output in order to support multiple versions of [GNU *binutils*](https://sourceware.org/binutils/docs/binutils/).&nbsp;([#976760](https://bugs.debian.org/976760))
    * Don't show any progress indicators when running [`zstd`](https://github.com/facebook/zstd).&nbsp;([#226](https://salsa.debian.org/reproducible-builds/diffoscope/issues/226))
    * Correct the grammatical tense in the `--debug` log output.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/456f6c3)]

* Codebase improvements:

    * Update the `debian/copyright` file to match the copyright notices in the source tree.&nbsp;([#224](https://salsa.debian.org/reproducible-builds/diffoscope/issues/224))
    * Update various years across the codebase in `.py` copyright headers.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/95f0ba8)]
    * Rewrite the filter routine that post-processes the output from `readelf(1)`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7902107)]
    * Remove unnecessary [PEP 263](https://www.python.org/dev/peps/pep-0263/) encoding header lines; unnecessary after [PEP 3120](https://www.python.org/dev/peps/pep-3120/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ee2b245)]
    * Use `minimal` instead of `basic` as a variable name to match the underlying package name.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/2280450)]
    * Use [`pprint.pformat`](https://docs.python.org/3/library/pprint.html#pprint.pformat) in the JSON comparator to serialise the differences from [`jsondiff`](http://www.jsondiff.com/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/41360e5)]

In addition, Jean-Romain Garnier added tests for OpenJDK 14.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/06cb774)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/77bc7d5)]

In [disorderfs](https://tracker.debian.org/pkg/disorderfs) (our [FUSE](https://en.wikipedia.org/wiki/Filesystem_in_Userspace)-based filesystem that deliberately introduces non-determinism into directory system calls in order to flush out reproducibility issues), Chris Lamb added support for testing on [Salsa](https://salsa.debian.org)'s CI system [[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/869ec00)][[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/1e29fbf)][[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/488ebb7)] and added a quick benchmark [[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/a4503fe)]. For the [GNU Guix](https://guix.gnu.org/) distribution, Vagrant Cascadian *diffoscope* to version 162&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=c8ea8516d08d3defd694cd3991595e8a2747899a)].

## Homepage/documentation updates

There were a number of updates to the [main Reproducible Builds website and documentation](https://reproducible-builds.org/) this month, including:

* Calum McConnell fixed a broken link.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/727c06e)]

* Chris Lamb applied a typo fix from Roland Clobus [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/77db29a)], fixed the draft detection logic ([#28](https://salsa.debian.org/reproducible-builds/reproducible-website/issues/28)), added more academic articles to our list [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/cade061)] and corrected a number of grammar issues [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/0f34654)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/690fec9)].

* Holger Levsen documented the `#reproducible-changes`, `#debian-reproducible-changes` and `#archlinux-reproducible` IRC channels.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/b65a701)]

* *kpcyrd* added [`rebuilderd`](https://github.com/kpcyrd/rebuilderd) and [`archlinux-repro`](https://github.com/archlinux/archlinux-repro) to the [list of tools]({{ "/tools" | relative_url }}).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/57e0e1d)]

## Testing framework

[![]({{ "/images/reports/2020-11/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a large [Jenkins](https://jenkins.io/)-based testing framework that powers [`tests.reproducible-builds.org`](https://tests.reproducible-builds.org). This month, Holger Levsen made the following changes:

* [Debian](https://debian.org/)-related changes:

    * Update code copy of `debrebuild`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2619b67b)]
    * Add Debian *sid* `sources.list` entry on a node for test rebuilds.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/deaf90b4)]
    * Use *focal* instead of the (deprecated) *eoan* release for hosts running Ubuntu.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9c33f609)]

* [Jenkins](https://jenkins.io/) administration:

    * Show update frequency on the Jenkins shell monitor.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5d2c57b2)]
    * In the Jenkins shutdown monitor, force precedence to find only log files.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ea20af8e)]
    * Update `/etc/init.d` script from the latest `jenkins` package.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ac0bedf8)]

* System health checks & notifications:

    * Detect database locks in the [*pacman*](https://wiki.archlinux.org/index.php/pacman) Arch Linux package manager.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2e0415c7)]
    * Detect hosts running in the 'wrong future'.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6dadb32e)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/52bb012e)]
    * Install the [`apt-utils`](https://tracker.debian.org/pkg/apt-utils) package on all Debian-based hosts.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a136686a)]
    * Use [`/citests/`]({{ "/citests/" | relative_uri }}) as the landing page.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/35fea5fb)]
    * Update our `debrebuild` fork.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8e00fda3)]

In addition, Mattia Rizzolo made some [Debian](https://debian.org/)-related changes, including refreshing the [GnuPG](https://gnupg.org/) key of our repository [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/67989e71)], dropping some unused code [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d77872ba)] and skipping `pbuilder`/`sdist` updates on nodes that are not performing Debian-related rebuilds [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4e8e7d51)].&nbsp;[Marcus Hoffmann](https://bubu1.eu/) updated his mailing list subscription status too [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/dc439d4d)].

Lastly, build node maintenance was also performed by Holger Levsen [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/79667d49)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/817efb4a)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f77c8a48)], Mattia Rizzolo [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f38d116f)] and Vagrant Cascadian [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/26927d3b)].

## Upstream patches

The following patches were created this month:

* Arnout Engelen:

    * [`git`](https://lore.kernel.org/git/20201201095037.20715-1-arnout@bzzt.net/T/#u) (reproducible manual)

* Bernhard M. Wiedemann:

    * [`breezy`](https://github.com/breezy-team/breezy/pull/119) (partial fix for build failing in the future)
    * [`cherrytree`](https://github.com/giuspen/cherrytree/pull/1401) (embeds date)
    * [`diff-pdf`](https://bugzilla.opensuse.org/show_bug.cgi?id=1180471) (manual package had error with time)
    * [`flnews`](https://build.opensuse.org/request/show/859515) (date)
    * [`goxel`](https://build.opensuse.org/request/show/858666) (filesystem ordering issue)
    * [`java-16-openjdk`](http://bugs.java.com/bugdatabase/view_bug.do?bug_id=JDK-8258190) (date/copyright year issue)
    * [`minikube`](https://build.opensuse.org/request/show/856786) (embeds date and time)
    * [`monero`](https://github.com/monero-project/monero/pull/7241) (CPU detection)
    * [`mono-core`](https://github.com/mono/mono/pull/20690) (embeds date)
    * [`plymouth`](https://build.opensuse.org/request/show/853457) (changelog date is not parsable)
    * [`python-veusz`](https://github.com/veusz/veusz/pull/479) (filesystem ordering issue)
    * [`rabbitmq-c`](https://github.com/alanxz/rabbitmq-c/pull/638) (does not use CMake `TIMESTAMP` for date in manpage)
    * [`rubygem-passenger`](https://github.com/phusion/passenger/pull/2328) (filesystem ordering issue)
    * [`spack`](https://github.com/spack/spack/pull/20629) (parallelism-related issue)
    * [`sysdig`](https://github.com/draios/sysdig/pull/1717) (fix build failure on `i586`)
    * [`xmlgraphics-fop`](https://github.com/apache/xmlgraphics-fop/pull/65) (embeds date issue)

* Chris Lamb:

    * [#976445](https://bugs.debian.org/976445) filed against [`libjs-qunit`](https://tracker.debian.org/pkg/libjs-qunit).
    * [#976447](https://bugs.debian.org/976447) filed against [`circlator`](https://tracker.debian.org/pkg/circlator).
    * [#976449](https://bugs.debian.org/976449) filed against [`python-pyramid`](https://tracker.debian.org/pkg/python-pyramid).
    * [#976715](https://bugs.debian.org/976715) filed against [`eric`](https://tracker.debian.org/pkg/eric).
    * [#976716](https://bugs.debian.org/976716) filed against [`jbbp`](https://tracker.debian.org/pkg/jbbp).
    * [#976826](https://bugs.debian.org/976826) filed against [`dvbstreamer`](https://tracker.debian.org/pkg/dvbstreamer).
    * [#976827](https://bugs.debian.org/976827) filed against [`knot-resolver`](https://tracker.debian.org/pkg/knot-resolver).
    * [#977486](https://bugs.debian.org/977486) filed against [`sayonara`](https://tracker.debian.org/pkg/sayonara).
    * [#977487](https://bugs.debian.org/977487) filed against [`pyvows`](https://tracker.debian.org/pkg/pyvows).
    * [#977488](https://bugs.debian.org/977488) filed against [`osmo-mgw`](https://tracker.debian.org/pkg/osmo-mgw).
    * [#978010](https://bugs.debian.org/978010) filed against [`mail-expire`](https://tracker.debian.org/pkg/mail-expire).

* Vagrant Cascadian:

    * [#976307](https://bugs.debian.org/976307) filed against [`sudo`](https://tracker.debian.org/pkg/sudo).
    * [#976348](https://bugs.debian.org/976348) filed against [`debian-faq`](https://tracker.debian.org/pkg/debian-faq).
    * [#976359](https://bugs.debian.org/976359) filed against [`debian-history`](https://tracker.debian.org/pkg/debian-history).
    * [#976898](https://bugs.debian.org/976898) filed against [`xorg-server`](https://tracker.debian.org/pkg/xorg-server).
    * [#977001](https://bugs.debian.org/977001) & [#977003](https://bugs.debian.org/977003) filed against [`blitz++`](https://tracker.debian.org/pkg/blitz++).
    * [#977018](https://bugs.debian.org/977018) filed against [`deltachat-core`](https://tracker.debian.org/pkg/deltachat-core).
    * [#977151](https://bugs.debian.org/977151), [#977153](https://bugs.debian.org/977153) & [#977154](https://bugs.debian.org/977154) filed against [`cctools`](https://tracker.debian.org/pkg/cctools).
    * [#977167](https://bugs.debian.org/977167) & [#977168](https://bugs.debian.org/977168) filed against [`west-chamber`](https://tracker.debian.org/pkg/west-chamber).
    * [#977171](https://bugs.debian.org/977171) filed against [`seqan3`](https://tracker.debian.org/pkg/seqan3).
    * [#977177](https://bugs.debian.org/977177) filed against [`mm-common`](https://tracker.debian.org/pkg/mm-common).
    * [#977179](https://bugs.debian.org/977179) & [#977180](https://bugs.debian.org/977180) filed against [`mlpost`](https://tracker.debian.org/pkg/mlpost).
    * [#977319](https://bugs.debian.org/977319) filed against [`slurm-wlm`](https://tracker.debian.org/pkg/slurm-wlm).
    * [#977330](https://bugs.debian.org/977330) filed against [`uhd`](https://tracker.debian.org/pkg/uhd).
    * [#977412](https://bugs.debian.org/977412) filed against [`xastir`](https://tracker.debian.org/pkg/xastir).
    * [#977415](https://bugs.debian.org/977415) & [#977416](https://bugs.debian.org/977416) filed against [`klayout`](https://tracker.debian.org/pkg/klayout).
    * [#977424](https://bugs.debian.org/977424) & [#977425](https://bugs.debian.org/977425) filed against [`gnuradio`](https://tracker.debian.org/pkg/gnuradio).
    * [#977428](https://bugs.debian.org/977428) filed against [`lprng`](https://tracker.debian.org/pkg/lprng).
    * [#977431](https://bugs.debian.org/977431) filed against [`tortoize`](https://tracker.debian.org/pkg/tortoize).
    * [#977432](https://bugs.debian.org/977432) & [#977433](https://bugs.debian.org/977433) filed against [`kyua`](https://tracker.debian.org/pkg/kyua).
    * [#977435](https://bugs.debian.org/977435) & [#977436](https://bugs.debian.org/977436) filed against [`bowtie2`](https://tracker.debian.org/pkg/bowtie2).
    * [#977521](https://bugs.debian.org/977521) & [#977522](https://bugs.debian.org/977522) filed against [`vtk9`](https://tracker.debian.org/pkg/vtk9).
    * [#977529](https://bugs.debian.org/977529), [#977530](https://bugs.debian.org/977530) & [#977531](https://bugs.debian.org/977531) filed against [`vtk7`](https://tracker.debian.org/pkg/vtk7).
    * [#977553](https://bugs.debian.org/977553) filed against [`embree`](https://tracker.debian.org/pkg/embree).
    * [#977561](https://bugs.debian.org/977561) filed against [`osmo-hlr`](https://tracker.debian.org/pkg/osmo-hlr).
    * [#977609](https://bugs.debian.org/977609) filed against [`grap`](https://tracker.debian.org/pkg/grap).
    * [#977610](https://bugs.debian.org/977610) filed against [`webauth`](https://tracker.debian.org/pkg/webauth).
    * [#977614](https://bugs.debian.org/977614) filed against [`ircd-irc2`](https://tracker.debian.org/pkg/ircd-irc2).
    * [#977618](https://bugs.debian.org/977618) filed against [`dssp`](https://tracker.debian.org/pkg/dssp).
    * [#977641](https://bugs.debian.org/977641) filed against [`fckit`](https://tracker.debian.org/pkg/fckit).
    * [#977684](https://bugs.debian.org/977684) filed against [`mahimahi`](https://tracker.debian.org/pkg/mahimahi).
    * [#977747](https://bugs.debian.org/977747) & [#977793](https://bugs.debian.org/977793) filed against [`nis`](https://tracker.debian.org/pkg/nis).
    * [#977783](https://bugs.debian.org/977783) & [#977784](https://bugs.debian.org/977784) filed against [`magicfilter`](https://tracker.debian.org/pkg/magicfilter).
    * [#977999](https://bugs.debian.org/977999) filed against [`libcifpp`](https://tracker.debian.org/pkg/libcifpp).
    * [#978003](https://bugs.debian.org/978003) filed against [`density-fitness`](https://tracker.debian.org/pkg/density-fitness).
    * [#978009](https://bugs.debian.org/978009) filed against [`calife`](https://tracker.debian.org/pkg/calife).
    * [#978044](https://bugs.debian.org/978044) filed against [`wily`](https://tracker.debian.org/pkg/wily).
    * [#978054](https://bugs.debian.org/978054) filed against [`autoconf`](https://tracker.debian.org/pkg/autoconf).
    * [#978064](https://bugs.debian.org/978064) filed against [`libdigidoc`](https://tracker.debian.org/pkg/libdigidoc).
    * [#978067](https://bugs.debian.org/978067) & [#978392](https://bugs.debian.org/978392) filed against [`epm`](https://tracker.debian.org/pkg/epm).
    * [#978088](https://bugs.debian.org/978088) filed against [`rheolef`](https://tracker.debian.org/pkg/rheolef).
    * [#978407](https://bugs.debian.org/978407) & [#978408](https://bugs.debian.org/978408) filed against [`libcommoncpp2`](https://tracker.debian.org/pkg/libcommoncpp2).
    * [#978494](https://bugs.debian.org/978494) filed against [`binutils-arm-none-eabi`](https://tracker.debian.org/pkg/binutils-arm-none-eabi).
    * [#978499](https://bugs.debian.org/978499) filed against [`fop`](https://tracker.debian.org/pkg/fop).
    * [#978577](https://bugs.debian.org/978577) & [#978578](https://bugs.debian.org/978578) filed against [`coinor-symphony`](https://tracker.debian.org/pkg/coinor-symphony).
    * [#978598](https://bugs.debian.org/978598) filed against [`net-snmp`](https://tracker.debian.org/pkg/net-snmp).
    * [#978609](https://bugs.debian.org/978609) & [#978610](https://bugs.debian.org/978610) filed against [`a2ps`](https://tracker.debian.org/pkg/a2ps).
    * [#978734](https://bugs.debian.org/978734) filed against [`mp3blaster`](https://tracker.debian.org/pkg/mp3blaster).
    * [#978746](https://bugs.debian.org/978746) filed against [`minlog`](https://tracker.debian.org/pkg/minlog).
    * [#978751](https://bugs.debian.org/978751) filed against [`milter-greylist`](https://tracker.debian.org/pkg/milter-greylist).
    * [#978946](https://bugs.debian.org/978946) filed against [`gfxboot`](https://tracker.debian.org/pkg/gfxboot).

---

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter/Mastodon: [@ReproBuilds](https://twitter.com/ReproBuilds) / [@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)

 * Reddit: [/r/ReproducibleBuilds](https://reddit.com/r/reproduciblebuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
