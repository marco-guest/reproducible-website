---
layout: report
year: "2020"
month: "08"
title: "Reproducible Builds in August 2020"
draft: false
date: 2020-09-09 18:00:39
---

**Welcome to the August 2020 report from the [Reproducible Builds](https://reproducible-builds.org) project.**

[![]({{ "/images/reports/2020-08/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

In our monthly reports, we summarise the things that we have been up to over the past month. The motivation behind the Reproducible Builds effort is to ensure no flaws have been introduced from the original free software source code to the pre-compiled binaries we install on our systems. If you're interested in contributing to the project, [please visit our main website]({{ "/" | relative_url }}).

<br>

----

<br>

This month, [Jennifer Helsby](https://redshiftzero.github.io/) launched a new [*reproduciblewheels.com*](https://reproduciblewheels.com/) website to address the lack of reproducibility of [Python wheels](https://pythonwheels.com/).

To quote Jennifer's [accompanying explanatory blog post](https://redshiftzero.github.io/reproducible-wheels/):

> One hiccup we've encountered in [SecureDrop](https://securedrop.org/) development is that not all Python wheels can be built reproducibly. We ship multiple (Python) projects in Debian packages, with Python dependencies included in those packages as wheels. In order for our Debian packages to be reproducible, we need that wheel build process to also be reproducible

Parallel to this, [*transparencylog.com*](https://www.transparencylog.com/) was also launched, a service that verifies the contents of URLs against a publicly recorded cryptographic log. It keeps an [append-only log](https://en.wikipedia.org/wiki/Append-only) of the cryptographic digests of all URLs it has seen. ([GitHub repo](https://github.com/transparencylog/tl))

[![]({{ "/images/reports/2020-08/isdd2020.png#right" | relative_url }})](https://www.eco.de/events/internet-security-days-2020/isdd-2020-agenda/#best_practises__aus_erfahrungen_lernen)

On 18th September, Bernhard M. Wiedemann will give a presentation in German, titled [*Wie reproducible builds Software sicherer machen*](https://www.eco.de/events/internet-security-days-2020/isdd-2020-agenda/#best_practises__aus_erfahrungen_lernen) ("How reproducible builds make software more secure") at the [Internet Security Digital Days 2020](https://www.eco.de/events/internet-security-days-2020/) conference.

<br>

### Reproducible builds at DebConf20

There were a number of talks at the recent online-only [DebConf20](https://debconf20.debconf.org/) conference on the topic of reproducible builds.

[![]({{ "/images/reports/2020-08/debconf20-holger.jpg#center" | relative_url }})](https://debconf20.debconf.org/talks/49-reproducing-bullseye-in-practice/)

Holger gave a talk titled "[*Reproducing Bullseye in practice*](https://debconf20.debconf.org/talks/49-reproducing-bullseye-in-practice/)", focusing on independently verifying that the binaries distributed from `ftp.debian.org` are made from their claimed sources. It also served as a general update on the status of reproducible builds within Debian. The [video](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/49-reproducing-bullseye-in-practice.webm) (145 MB) and [slides](https://reproducible-builds.org/_lfs/presentations/2020-08-27-Reproducing-bullseye-in-practice/) are available.

[![]({{ "/images/reports/2020-08/debconf20.png#right" | relative_url }})](https://debconf20.debconf.org/)

There were also a number of other talks that involved Reproducible Builds too. For example, the Malayalam language mini-conference had a talk titled [*എനിയ്ക്കും ഡെബിയനില്‍ വരണം, ഞാന്‍ എന്തു് ചെയ്യണം?*](https://debconf20.debconf.org/talks/74-i-want-to-join-debian-what-should-i-do/) ("I want to join Debian, what should I do?") presented by Praveen Arimbrathodiyil, the [Clojure Packaging Team BoF](https://debconf20.debconf.org/talks/33-clojure-packaging-team-bof/) session led by [Elana Hashman](https://hashman.ca/), as well as [*Where is Salsa CI right now?*](https://debconf20.debconf.org/talks/47-where-is-salsa-ci-right-now/) that was on the topic of [Salsa](http://salsa.debian.org/), the collaborative development server that Debian uses to provide the necessary tools for package maintainers, packaging teams and so on.

Jonathan Bustillos (*Jathan*) also gave a talk in Spanish titled [*Un camino verificable desde el origen hasta el binario*](https://debconf20.debconf.org/talks/81-construcciones-reproducibles-en-debian-debian-reproducible-builds-un-camino-verificable-desde-el-origen-hasta-el-binario/) ("A verifiable path from source to binary"). ([Video](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/81-construcciones-reproducibles-en-debian-debian-reproducible-builds-un-camino-verificable-desde-el-origen-hasta-el-binario.webm), 88MB)

<br>

## Development work

[![]({{ "/images/reports/2020-08/rust.jpg#right" | relative_url }})](https://www.rust-lang.org/)

After [many years of development work](https://github.com/rust-lang/rust/issues/34902), the compiler for the [Rust programming language](https://www.rust-lang.org/) now generates reproducible binary code. This generated [some general discussion on Reddit](https://www.reddit.com/r/rust/comments/i4ij47/rustc_1441_is_reproducible_in_debian/) on the topic of reproducibility in general.

Paul Spooren posted a 'request for comments' to [OpenWrt](https://openwrt.org)'s [`openwrt-devel`](https://lists.openwrt.org/pipermail/openwrt-devel/2020-August/030747.html) mailing list asking for clarification on when to raise the `PKG_RELEASE` identifier of a package. This is needed in order to successfully perform rebuilds in a reproducible builds context.

[![]({{ "/images/reports/2020-08/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

In [openSUSE](https://www.opensuse.org/), Bernhard M. Wiedemann published his [monthly Reproducible Builds status update](https://lists.opensuse.org/opensuse-factory/2020-08/msg00355.html).

Chris Lamb provided some comments and pointers on an upstream issue regarding the reproducibility of a [Snap](https://snapcraft.io/) / [SquashFS](https://en.wikipedia.org/wiki/SquashFS) archive file.&nbsp;[[...](https://github.com/yakshaveinc/linux/issues/39)]

#### [Debian](https://debian.org/)

Holger Levsen identified that a large number of Debian `.buildinfo` build certificates have been "tainted" on the official Debian build servers, as these environments have files underneath the `/usr/local/sbin` directory&nbsp;[[...](https://bugs.debian.org/969084)]. He also filed against bug for `debrebuild` after spotting that it can fail to download packages from [`snapshot.debian.org`](http://snapshot.debian.org/)&nbsp;[[...](https://bugs.debian.org/969098)].

[![]({{ "/images/reports/2020-08/debian.png#right" | relative_url }})](https://debian.org/)

This month, several issues were uncovered (or assisted) due to the efforts of reproducible builds.

For instance, Debian bug [#968710](https://bugs.debian.org/968710) was filed by Simon McVittie, which describes a problem with [detached debug symbol files](https://wiki.debian.org/DebugPackage) (required to [generate a traceback](https://wiki.debian.org/HowToGetABacktrace)) that is unlikely to have been discovered without reproducible builds. In addition, [Jelmer Vernooij](https://www.jelmer.uk/) called attention that the new [Debian Janitor](https://janitor.debian.net/) tool is using the property of reproducibility (as well as [diffoscope](https://diffoscope.org/) when applying archive-wide changes to Debian:

> New merge proposals also include a link to the diffoscope diff between a vanilla build and the build with changes. Unfortunately these can be a bit noisy for packages that are not reproducible yet, due to the difference in build environment between the two builds.&nbsp;[[...](https://www.jelmer.uk/janitor-update-1.html)]

56 reviews of Debian packages were added, 38 were updated and 24 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Specifically, Chris Lamb added and categorised the `nondeterministic_version_generated_by_python_param` and the `lessc_nondeterministic_keys` toolchain issues.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/d0aab73d)][[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/50e646d9)]

[![]({{ "/images/reports/2020-08/intoto.png#right" | relative_url }})](https://in-toto.io/)

Holger Levsen sponsored Lukas Puehringer's upload of the [python-securesystemslib](https://tracker.debian.org/pkg/python-securesystemslib) pacage, which is a dependency of [in-toto](https://in-toto.io/), a framework to secure the integrity of software supply chains.&nbsp;[[...](https://tracker.debian.org/news/1173060/accepted-python-securesystemslib-0160-1-source-into-unstable/)]


Lastly, Chris Lamb further refined his merge request against the `debian-installer` component to allow all arguments from `sources.list` files (such as `[check-valid-until=no]`) in order that we can test the reproducibility of the installer images on the [Reproducible Builds own testing infrastructure](https://tests.reproducible-builds.org/debian/reproducible.html) and [sent a ping to the team that maintains that code](https://lists.reproducible-builds.org/pipermail/rb-general/2020-August/002027.html).

<br>

## Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of these patches, including:

* Bernhard M. Wiedemann:

    * [`asymptote`](https://github.com/vectorgraphics/asymptote/pull/170) (shell/Perl date)
    * `getfem` (embeds datetime and user, submitted via email)
    * [`getdp`](https://build.opensuse.org/request/show/824994) (hostname and user)
    * [`getdp`](https://gitlab.onelab.info/getdp/getdp/-/issues/62) (user)
    * [`guix`](https://build.opensuse.org/request/show/826309) (disable parallelism)
    * [`httpcomponents-client`](https://bugzilla.opensuse.org/show_bug.cgi?id=1174795) (Java documentation generator `readdir` order)
    * [`kuberlr`](https://github.com/flavio/kuberlr/pull/6) (date)
    * `lal` (date and time issue, submitted via email)
    * [`libmesh`](https://build.opensuse.org/request/show/825015) (host)
    * [`OBS`](https://github.com/openSUSE/open-build-service/issues/10020) (discuss how to track old build `prjconf` metadata in buildinfo)
    * [`openblas`](https://build.opensuse.org/request/show/825866) (disable CPU detection)
    * [`openfoam-selector`](https://develop.openfoam.com/Community/feature-scripts/-/issues/2) (date)
    * [`perl`](https://github.com/perl-pod/pod-simple/pull/120) (toolchain, date)
    * [`python-blosc`](https://build.opensuse.org/request/show/828284) (CPU detection)
    * [`python-eventlet`](https://github.com/eventlet/eventlet/pull/643) (fails to build far in the future)
    * [`rna-star`](https://build.opensuse.org/request/show/825019) (date and hostname)
    * [`trilinos`](https://github.com/trilinos/Trilinos/pull/7814) (date)
    * [`xz/b4`](https://github.com/openSUSE/obs-service-recompress/pull/17) (workaround CPU count influencing output, [reported upstream](https://www.mail-archive.com/xz-devel@tukaani.org/msg00375.html))

* Benjamin Hof:

    * [`flit`](https://github.com/takluyver/flit/pull/366)

* Chris Lamb:

    * [#966657](https://bugs.debian.org/966657) filed against [`json-c`](https://tracker.debian.org/pkg/json-c) ([forwarded upstream](https://github.com/json-c/json-c/pull/653)).
    * [#967238](https://bugs.debian.org/967238) filed against [`nmh`](https://tracker.debian.org/pkg/nmh).
    * [#968045](https://bugs.debian.org/968045) filed against [`golang-gonum-v1-plot`](https://tracker.debian.org/pkg/golang-gonum-v1-plot).
    * [#968183](https://bugs.debian.org/968183) filed against [`chirp`](https://tracker.debian.org/pkg/chirp).
    * [#968185](https://bugs.debian.org/968185) filed against [`pixelmed-codec`](https://tracker.debian.org/pkg/pixelmed-codec).
    * [#968187](https://bugs.debian.org/968187) filed against [`debhelper`](https://tracker.debian.org/pkg/debhelper).
    * [#968189](https://bugs.debian.org/968189) filed against [`muroar`](https://tracker.debian.org/pkg/muroar).
    * [#968278](https://bugs.debian.org/968278) filed against [`serd`](https://tracker.debian.org/pkg/serd).
    * [#968344](https://bugs.debian.org/968344) filed against [`pencil2d`](https://tracker.debian.org/pkg/pencil2d).
    * [#968557](https://bugs.debian.org/968557) filed against [`tpot`](https://tracker.debian.org/pkg/tpot).
    * [#968700](https://bugs.debian.org/968700) filed against [`evolution`](https://tracker.debian.org/pkg/evolution).
    * [#969320](https://bugs.debian.org/969320) filed against [`aflplusplus`](https://tracker.debian.org/pkg/aflplusplus).

* Hervé Boutemy:

    * [`plexus-archiver`](https://github.com/codehaus-plexus/plexus-archiver/issues/127) (timezone/DST issue)

* Vagrant Cascadian:

    * [#968627](https://bugs.debian.org/968627) filed against [`libjpeg-turbo`](https://tracker.debian.org/pkg/libjpeg-turbo).
    * [#968641](https://bugs.debian.org/968641) filed against [`jack-audio-connection-kit`](https://tracker.debian.org/pkg/jack-audio-connection-kit).
    * [#968652](https://bugs.debian.org/968652) filed against [`glusterfs`](https://tracker.debian.org/pkg/glusterfs).


### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2020-08/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility that can not only locate and diagnose reproducibility issues, it provides human-readable diffs of all kinds. In August, Chris Lamb made the following changes to [diffoscope](https://diffoscope.org), including preparing and uploading versions `155`, `156`, `157` and `158` to Debian:

* New features:

    * Support extracting data of PGP signed data. ([#214](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/214))
    * Try files named `.pgp` against `pgpdump(1)` to determine whether they are Pretty Good Privacy (PGP) files. ([#211](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/211))
    * Support multiple options for all file extension matching.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/aa2e220)]

* Bug fixes:

    * Don't raise an exception when we encounter XML files with `<!ENTITY>` declarations inside the [Document Type Definition](https://en.wikipedia.org/wiki/Document_type_definition) (DTD), or when a DTD or entity references an external resource. ([#212](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/212))
    * `pgpdump(1)` can successfully parse some binary files, so check that the parsed output contains something sensible before accepting it.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/156f239)]
    * Temporarily drop `gnumeric` from the Debian build-dependencies as it has been removed from the *testing* distribution. ([#968742](https://bugs.debian.org/968742))
    * Correctly use `fallback_recognises` to prevent matching `.xsb` binary XML files.
    * Correct identify signed PGP files as `file(1)` returns "`data`". ([#211](https://salsa.debian.org/reproducible-builds/diffoscope/-/issues/211))

* Logging improvements:

    * Emit a message when `ppudump` version does not match our file header.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8ce4515)]
    * Don't use Python's [`repr(object)`](https://docs.python.org/3/library/functions.html#repr) output in "Calling external command" messages.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/1c13eb0)]
    * Include the filename in the "... not identified by any comparator" message.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/821f4de)]

* Codebase improvements:

    * Bump Python requirement from 3.6 to 3.7. Most distributions are either shipping with Python 3.5 or 3.7, so supporting 3.6 is not only somewhat unnecessary but also cumbersome to test locally.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ec05101)]
    * Drop some unused imports&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/41da8aa)], drop an unnecessary dictionary comprehensions&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/4bf12b0)] and some unnecessary control flow&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/bc22a4c)].
    * Correct typo of "output" in a comment.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/2853d1e)]

* Release process:

    * Move generation of `debian/tests/control` to an external script.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/cd15dfc)]
    * Add some URLs for the site that will appear on [PyPI.org](https://pypi.org/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7856b3c)]
    * Update "author" and "author email" in `setup.py` for [PyPI.org](https://pypi.org/) and similar.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0023819)]

* Testsuite improvements:

    * Update PPU tests for compatibility with Free Pascal versions 3.2.0 or greater. ([#968124](https://bugs.debian.org/968124))
    * Mark that our identification test for `.ppu` files requires `ppudump` version 3.2.0 or higher.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/65dde92)]
    * Add an assert_diff helper that loads and compares a fixture output.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/90fe3f3)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d76a231)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/4e61d0a)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a764a09)]

* Misc:

    * Duplicate docker instructions in the *Get diffoscope* section of the [diffoscope website](https://diffoscope.org/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/d7b1090)]

In addition, Mattia Rizzolo documented in `setup.py` that *diffoscope* works with Python version 3.8&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6f54fb4)] and Frazer Clews applied some [Pylint](https://en.wikipedia.org/wiki/Pylint) suggestions&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/26d9a39)] and removed some deprecated methods&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/13c9654)].


### [Website](https://reproducible-builds.org/)

[![]({{ "/images/reports/2020-08/website.png#right" | relative_url }})](https://reproducible-builds.org/)

This month, Chris Lamb updated the [main Reproducible Builds website and documentation](https://reproducible-builds.org/) to:

* Clarify & fix a few entries on the "who" page&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/1aae193)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/7cc3ae2)] and ensure that images do not get to large on some viewports&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/314b6d6)].
* Clarify use of a pronoun re. Conservancy.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/8f535a2)]
* Use "View all our monthly reports" over "View all monthly reports".&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/1d746ba)]
* Move a "is a" suffix out of the link target on the [`SOURCE_DATE_EPOCH`](https://reproducible-builds.org/docs/source-date-epoch/) age.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/8a333b1)]

In addition, Javier Jardón added the [freedesktop-sdk](https://freedesktop-sdk.io/) project&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/db55a98)] and Kushal Das added [SecureDrop](https://securedrop.org/) project&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/58813d5)] to our [projects page]({{ "/who/" | relative_url }}). Lastly, Michael Pöhn added internationalisation and translation support with help from Hans-Christoph Steiner&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/-/merge_requests/56)].

## Testing framework

[![]({{ "/images/reports/2020-08/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operate a [Jenkins](https://jenkins.io/)-based testing framework to power [`tests.reproducible-builds.org`](https://tests.reproducible-builds.org). This month, Holger Levsen made the following changes:

* System health checks:

    * Improve explanation how the status and scores are calculated.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1c631d08)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/096346df)]
    * Update and condense view of detected issues.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0fec191d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b0c0ef61)]
    * Query the canonical configuration file to determine whether a job is disabled instead of duplicating/hardcoding this.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2cbe952e)]
    * Detect several problems when updating the status of reporting-oriented 'metapackage' sets.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b4d1083c)]
    * Detect when [diffoscope](https://diffoscope.org) is not installable &nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c2267e88)] and failures in DNS resolution&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/40109bfe)].

* Debian:

    * Update the URL to the [Debian security team bug tracker](http://security-tracker.debian.org/)'s Git repository.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/46d5d72b)]
    * Reschedule the *unstable* and *bullseye* distributions often for the `arm64` architecture.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5c73b8a6)]
    * Schedule *buster* less often for `armhf`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4dcaa029)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/df15eea4)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8dbbe93a)]
    * Force the build of certain packages in the work-in-progress package rebuilder.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/849a32f2)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e0ffbcf7)]
    * Only update the *stretch* and *buster* base build images when necessary.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/183ab710)]

* Other distributions:

    * For [F-Droid](https://f-droid.org/), trigger jobs by commits, not by a timer.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e0e85320)]
    * Disable the [Archlinux](https://www.archlinux.org/) HTML page generation job as it has never worked.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/90a3ce91)]
    * Disable the alternative [OpenWrt](https://openwrt.org) rebuilder jobs.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4654af22)]


* Misc;

    * Improve monitoring, such as number of mounts, disk, memory, etc..&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3d20adbf)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b173ca56)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/22a6ae1e)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7bcf46ae)]
    * Install the `ruby-jekyll-polyglot` package to needed for the recently-added [internationalisation and translation support](https://salsa.debian.org/reproducible-builds/reproducible-website/-/merge_requests/56) on the [Reproducible Builds website](https://reproducible-builds.org).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0b206a97)]
    * Update link to report potential issues.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a79f1967)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/afc93af9)]

Many other changes were made too, including:

* Chris Lamb:

    * Use `<pre>` HTML tags when dumping fixed-width debugging data in the 'self-serve' package scheduler.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e8e60b53)]

* Mattia Rizzolo:

    * For [Alpine](https://alpinelinux.org/) and [ArchLinux](https://www.archlinux.org/), make the cleanup routines in the event of an error more robust.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c0736467)]
    * Update the [sudo](https://www.sudo.ws/) configuration to permit Jenkins itself to unmount more directories.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f5d604d7)]
    * Setup automatic renewal of our [Let's Encrypt](https://letsencrypt.org/) certificates for all domains served by us (including [`jenkins.debian.net`](https://jenkins.debian.net), [`www.reproducible-builds.org`](https://www.reproducible-builds.org), [`diffoscope.org`](https://diffoscope.org), [`buildinfos.debian.net`](https://buildinfos.debian.net/), etc.).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/65be8512)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0c6c9c76)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c00d2ab8)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/97005e8b)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/da9520f9)]

* Vagrant Cascadian:

    * Mark that the [u-boot](https://www.denx.de/wiki/U-Boot) Universal Boot Loader should not build architecture independent packages on the `arm64` architecture anymore.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1fbb2799)]

Finally, build node maintenance was performed by Holger Levsen&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d919db69)], Mattia Rizzolo&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7751ca43)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/753fb976)] and Vagrant Cascadian&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3ff75861)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7c02bd79)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8d20390b)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/06378283)]

---

## Mailing list

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month, Leo Wandersleb sent a message to the list after he was wondering how to expand his [WalletScrutiny.com](https://walletscrutiny.com/) project (which aims to improve the security of Bitcoin wallets) from Android wallets to also monitor Linux wallets as well:

> If you think you know how to spread the word about reproducibility in the context of Bitcoin wallets through WalletScrutiny, your contributions are highly welcome on [this PR](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/merge_requests/68)&nbsp;[[...](https://lists.reproducible-builds.org/pipermail/rb-general/2020-August/002004.html)]

Julien Lepiller posted to the list linking to a blog post by Tavis Ormandy titled [*You don't need reproducible builds*](https://lists.reproducible-builds.org/pipermail/rb-general/2020-August/002007.html). Morten Linderud (*foxboron*) responded with a clear rebuttal that Tavis was only considering the narrow use-case of proprietary vendors and closed-source software. He additionally noted that the criticism that reproducible builds cannot prevent against backdoors being deliberately introduced into the upstream source ("bugdoors") are decidedly (and deliberately) outside the scope of reproducible builds to begin with.

Chris Lamb included the Reproducible Builds mailing list in a wider discussion regarding a tentative [proposal to include `.buildinfo` files in `.deb` packages](https://wiki.debian.org/Teams/Dpkg/Spec/BundledBuildinfo), adding his remarks regarding requiring a custom tool in order to determine whether generated build artifacts are 'identical' in a reproducible context.&nbsp;[[...](https://lists.reproducible-builds.org/pipermail/rb-general/2020-August/002030.html)]

Jonathan Bustillos (*Jathan*) posted a quick email to the list requesting whether there was a list of [*To do tasks in Reproducible Builds*](https://lists.reproducible-builds.org/pipermail/rb-general/2020-August/002016.html).

[![]({{ "/images/reports/2020-08/tails.png#right" | relative_url }})](https://tails.boum.org/)

Lastly, Chris Lamb responded at length to a query regarding the status of reproducible builds for Debian ISO or installation images. He noted that most of the technical work has been performed but "there are at least four issues until they can be generally advertised as such". He pointed that the privacy-oriented [Tails](https://tails.boum.org/) operation system, which is based directly on Debian, has had reproducible builds for a number of years now.&nbsp;[[...](https://lists.reproducible-builds.org/pipermail/rb-general/2020-August/002018.html)]

<br>

---

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mastodon: [@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
