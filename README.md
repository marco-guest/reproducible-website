Source for the reproducible-builds.org website
==============================================

The website for reproducible-builds.org is made with [Jekyll]. It has
been initially created and tested with version 2.2.0 in Debian unstable
available in the `jekyll` package, though nowadays it also needs the
`ruby-jekyll-redirect-from` and `ruby-jekyll-polyglot` packages from
buster-backports or later.

[Jekyll]: https://jekyllrb.com/

Viewing the website
-------------------

It's possible to view the website while making changes:

    $ jekyll serve --watch

A local webserver will be started which can be accessed using a browser
to see changes as they are made.

Build the website
-----------------

Building the website is made by running:

```console
$ ./bin/i18n.sh  # optional, includes all the translations
$ jekyll build
```

The result will be available in the `_site` directory.

(Note: `./bin/i18n.sh` requires `po4a` >= v0.58, which is available in
buster-backports)

```console
$ sudo apt install po4a/buster-backports
```

Dependencies
------------

You can install the required tools and plugins and for building the website like this:

```console
$ sudo apt install jekyll git ruby-jekyll-redirect-from ruby-jekyll-polyglot/buster-backports po4a/buster-backports
```
